import { DemoTwoPageComponent } from './../demo-2/views/demo-two-page/demo-two-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'example-2',
    component: DemoTwoPageComponent
  },
  {
    path: '**',
    redirectTo: 'example-2',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class DemoTwoRoutingModule { }
