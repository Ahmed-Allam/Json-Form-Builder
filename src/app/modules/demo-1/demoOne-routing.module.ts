
import { SurveyPageComponent } from './views/survey-page/survey-page.component';
import { DemoOnePageComponent } from './views/demo-one-page/demo-one-page.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'survey',
  component: SurveyPageComponent
  },{
  path: 'example-1',
  component: DemoOnePageComponent
  },
  {
    path: '**',
    redirectTo: 'survey',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class DemoOneRoutingModule { }
