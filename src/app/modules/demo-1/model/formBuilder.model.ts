export interface FormModel {
  form : FormDataModel;
  formItems: FormItemModel[]
}

export interface FormItemModel {
  id?: string | number
  label?: string
  name: string
  type: string
  options?: MultiOptionModel[]
  required?: boolean
  placeholder?: string
  validation?: any
  value?: any
  controlWidth?: number,
  ratingOptions? : ratingModel
}

export interface MultiOptionModel {
  name: string
  value: string
}

export interface ratingModel {
  showText?: boolean
  titles? : string[]
  maxValue?: number

}

export interface FormDataModel {
  label?: string
  proceedAction: string
  cancelAction: string
}

