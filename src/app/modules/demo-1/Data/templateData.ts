import { FormItemModel } from '../model/formBuilder.model';
export const templateData: FormItemModel[] = [
  {
     "label":"text label",
     "placeholder": "text placeholder",
     "name":"textFormControlName",
     "type":"text",
     "controlWidth" : 1,
     "validation": {
       "required": true,
     }
  },
  {
    "label":"dropdown label",
    "name":"dropdownFormControlName",
    "type":"select",
    "controlWidth" : 1,
    "validation": {
      "required": true,
    },
    "options":[
       {
          "name":"option1",
          "value":"option1"
       },
       {
         "name":"option2",
         "value":"option2"
       },
       {
         "name":"option3",
         "value":"option3"
       },
       {
          "name":"option4",
          "value":"option4"
       }
    ]
 },
  {
    "label":"Email label",
    "placeholder": "Email Placeholder",
    "name":"emailFormControlName",
    "type":"email",
    "validation": {
     "type": "email"
   }
 },
  {
    "label":"textArea Label",
    "name":"textAreaFormControlName",
    "type":"TextArea",
    "placeholder": "Text Area placeholder...",
    "validation": {
      "min": 3,
    }
  },

  {
     "label":"Password Label",
     "name":"passwordFormControlName",
     "type":"password",
     "placeholder": "just another dummy password",
     "validation": {
      "min": 8
    }
  },
  {
    "label":"button 1",
    "name":"button1",
    "type":"button",
    "controlWidth" : 1,
},
{
  "label":"button 2",
  "name":"button2",
  "type":"button",
  "controlWidth" : 1,
},
  {
     "label":"checkbox label",
     "name":"radio",
     "type":"radio",
     "controlWidth" : 2,
     "validation": {
      "required": true,
      },
     "options":[
      {
          "name":"option1",
          "value":"option1"
      },
      {
        "name":"option2",
        "value":"option2"
      },
      {
        "name":"option3",
        "value":"option3"
      },
      {
          "name":"option4",
          "value":"option4"
      }
     ]
  },
  {
    "label":"checkbox label",
    "name":"check",
    "type":"check",
    "controlWidth" : 2,
    "validation": {
     "required": true,
   }
  }
]
