import { FormItemModel } from './../model/formBuilder.model';
export const fakeFormData: FormItemModel[] = [
  {
     "label":"Full Name",
     "placeholder": "Your Name",
     "name":"fullName",
     "type":"text",
     "controlWidth" : 1,
     "validation": {
       "required": true,
     }
  },
  {
    "label":"Position",
    "name":"position",
    "type":"select",
    "controlWidth" : 1,
    "validation": {
      "required": true,
    },
    "options":[
       {
          "name":"HR",
          "value":"hr"
       },
       {
         "name":"Senior Developer",
         "value":"seniorDeveloper"
       },
       {
         "name":"Technical Manager",
         "value":"technicalManager"
       },
       {
          "name":"CEO",
          "value":"ceo"
       }
    ]
 },
  {
    "label":"Email",
    "placeholder": "Your Email",
    "name":"email",
    "type":"email",
    "controlWidth" : 1,
    "validation": {
     "type": "email"
   }
 },
 {
  "label":"Password",
  "name":"password",
  "type":"password",
  "controlWidth" : 1,
  "placeholder": "just another dummy password",
  "validation": {
   "min": 8
 }
},
  {
    "label":"Message",
    "name":"textArea",
    "type":"textArea",
    "placeholder": "Tell me more...",
    "validation": {
      "min": 3,
    }
  },


  {
     "label":"How do you describe this demo ?",
     "name":"radio",
     "type":"radio",
     "controlWidth" : 1,
     "validation": {
      "required": true,
      },
     "options":[
        {
           "name":"Bad",
           "value":"bad"
        },
        {
           "name":"Mediocre",
           "value":"mediocre"
        },
        {
          "name":"Good",
          "value":"good"
       },
       {
          "name":"Awesome!",
          "value":"awesome"
       }
     ]
  },
  {
    "label":"I've tested all the project",
    "name":"check",
    "type":"check",
    "controlWidth" : 2,
    "validation": {
     "required": true,
   }
  },
  {
      "label":"Cancel",
      "name":"cancel",
      "type":"cancelButton",
  },
  {
      "label":"Submit",
      "name":"submit",
      "type":"submitButton",
  }
]
