import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { isMobile } from 'src/app/shared/helpers/is-mobile.helper';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  constructor() { }
  public isMobile: boolean = isMobile();

  ngOnInit(): void {

  }

}
