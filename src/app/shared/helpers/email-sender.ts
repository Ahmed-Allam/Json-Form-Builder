export function sendEmail(http, form): any {
  let submitted: boolean = false; // show and hide the success message
  let isLoading: boolean = false; // disable the submit button if we're loading
  let responseMessage: string; // the response message to show to the user
  http.post("https://script.google.com/macros/s/AKfycbwFJeR0Kk7u0uMWnwaQZ9yp1uM7BPj2vDnwgGl9-YqyxKnwYGP2xR0a/exec", form).subscribe(
  (response) => {
    // choose the response message
    if (response["result"] == "success") {
      responseMessage = "Thanks for the message! I'll get back to you soon!";
    } else {
      responseMessage = "Oops! Something went wrong... Reload the page and try again.";
    }
    submitted = true; // show the response message
    isLoading = false; // re enable the submit button
    console.log(response);
  },
  (error) => {
    responseMessage = "Oops! An error occurred... Reload the page and try again.";
    submitted = true; // show the response message
    isLoading = false; // re enable the submit button
    console.log(error);
  }
  )
  return {submitted, isLoading, responseMessage}
};
