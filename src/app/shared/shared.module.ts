import { ReactiveFormsModule } from '@angular/forms';
import { AceEditorModule } from 'ng2-ace-editor';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AngularSplitModule } from 'angular-split';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    AngularSplitModule,
    NgxJsonViewerModule,
    AceEditorModule,
    ReactiveFormsModule
  ],
  exports: [
    AngularSplitModule,
    NgxJsonViewerModule,
    AceEditorModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
