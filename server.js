// Express to run server and routes
const express = require('express');

// Start up an instance of app
const app = express();

/* Dependencies */
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Cors for cross origin allowance
const cors = require('cors');
app.use(cors());

app.use(express.static('./dist/Json-Form-Builder'));
app.get('/documentation', function(req, res) {
  res.sendFile('index.html', {root: 'documentation/'}
  );
});
app.get('/*', function(req, res) {
  res.sendFile('index.html', {root: 'dist/Json-Form-Builder/'}
  );
});



/* Initializing the main project folder */
// app.use(express.static('website'));
const port = 8080;

app.listen(process.env.PORT || port, listening);
function listening(){
    // console.log(server);
    console.log(`running on localhost: ${port}`);
};
